package ita.primaryteam.footballarchive.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import ita.primaryteam.footballarchive.domain.Club;
import ita.primaryteam.footballarchive.domain.Footballer;
import ita.primaryteam.footballarchive.domain.League;
import ita.primaryteam.footballarchive.repository.ClubRepository;
import ita.primaryteam.footballarchive.repository.FootballerRepository;
import ita.primaryteam.footballarchive.repository.LeagueRepository;
import ita.primaryteam.footballarchive.repository.MysqlClubRepository;
import ita.primaryteam.footballarchive.repository.MysqlFootballerRepository;
import ita.primaryteam.footballarchive.repository.MysqlLeagueRepository;

public class MenuUser {

	MysqlLeagueRepository mlr = new MysqlLeagueRepository();
	MysqlClubRepository mcr = new MysqlClubRepository();
	MysqlFootballerRepository mfr = new MysqlFootballerRepository();

	List<League> leagues = new ArrayList<League>();
	List<Club> clubs = new ArrayList<Club>();
	List<Footballer> footballers = new ArrayList<Footballer>();

	int choice;
	String user;

	public void clear() {
		for (int i = 0; i <= 50; i++) {
			System.out.println("");
		}
	}

	public void firstMenu(String user, String message) {
		
		this.user=user;
		
		int u = this.user.length();
		if(u < 38) {
			for( int i=0; i<(38-u); i++) {
				this.user = this.user + " ";
			}
		}

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*               MENU FOOTBALL                 *");
		System.out.println("*                                             *");
		System.out.println("*           1        Leagues                  *");
		System.out.println("*           2        Clubs                    *");
		System.out.println("*           3        Footballers              *");
		System.out.println("*           4        Exit                     *");
		System.out.println("*                                             *");

		this.waiting(message);

	}

	public void waiting(String message) {
		choice = 0;
		System.out.println("*************** Insert Choice  ****************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		while (true) {

			try {
				choice = Integer.parseInt(inputid.readLine());
			} catch (Exception e) {
				choice = 0;
			}

			switch (choice) {
			case 1:
				this.clear();
				this.printLeague("");
				break;
			case 2:
				this.clear();
				this.printClub("");
				break;
			case 3:
				this.clear();
				this.printFootballer("");
				break;
			case 4:
				System.out.println("End");
				System.exit(1);
			default:
				this.clear();
				this.firstMenu(user, "Error Choice");

			}
		}
	}

	public void printLeague(String message) {

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*                MENU LEAGUES                 *");
		System.out.println("*                                             *");
		System.out.println("*           1       Show List                 *");
		System.out.println("*           2       View League's Clubs       *");
		System.out.println("*           3       View League's Footballers *");
		System.out.println("*           4       Back Start Menu           *");
		System.out.println("*                                             *");
		System.out.println("*************** Insert Choice  ****************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewAllLeague();
			break;
		case 2:
			this.clear();
			this.viewClubs();
			break;
		case 3:
			this.clear();
			this.viewFootballers();
			break;
		case 4:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printLeague("Error Choice");
		}
	}

	public void addLeague() {
		League league = new League();

		System.out.println("***********************************************");
		System.out.println("*               INSERT LEAGUES                *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.print("Insert Name: ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {
			String nameL = input.readLine();
			league.setName(nameL);

			System.out.print("Insert Clubs Number: ");
			int clubNr;

			try {
				clubNr = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				clubNr = 0;
			}

			league.setClub(clubNr);

			System.out.print("Insert Nationality: ");

			String nationalityL = input.readLine();
			league.setNationality(nationalityL);

			LeagueRepository leagueRepository = new MysqlLeagueRepository();

			leagueRepository.add(league);

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*               INSERT LEAGUES                *");
			System.out.println("***********************************************");
			System.out.println("*               Entry Completed               *");
			System.out.println("*           Push Any Button For Back          *");
			System.out.println("***********************************************");

			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printLeague("");

	}

	public void viewAllLeague() {

		leagues = mlr.getAll();

		this.clear();
		
		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println("Push Any Button For Back");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printLeague("");
	}

	public void viewClubs() {
		leagues = mlr.getAll();

		this.clear();
		
		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println("Choice Id League: ");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		
		int id;
		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id=0;
		}
		
		clubs = mcr.getClubs(id);

		this.clear();
		
		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("Push Any Button For Back");

		inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printLeague("");
	}

	public void viewFootballers() {

		leagues = mlr.getAll();

		this.clear();
		
		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println(" Choice League id:");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			int idLeague = Integer.parseInt(inputid.readLine());

			footballers = mfr.getFootballers(idLeague);
		} catch (Exception e) {
			e.getMessage();
		}

		this.clear();
		
		System.out.println("******************************************************************************************************************************");
		System.out.println("*                                                    FOOTBALLERS                                                             *");
		System.out.println("*                                                                                                                            *");
		System.out.println("* Id     Name                  Surname                 Nr      Foot    Pos.     Nat.                    Club                 *");
		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");

		for (int i = 0; i < footballers.size(); i++) {
			System.out.println(footballers.get(i).toString());
		}

		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");
		System.out.println("Push Any Button For Back");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printLeague("");
	}

	public void updateLeague() {

		leagues = mlr.getAll();

		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println(" Choice League id:");

		League league = new League();

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id = 0;
		}

		System.out.println("***********************************************");
		System.out.println("*               UPDATE LEAGUES                *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.print("Update Name: ");

		try {
			league.setName(inputid.readLine());
		} catch (Exception e) {
		}

		System.out.print("Update Clubs Number: ");
		int clubNr;

		try {
			clubNr = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			clubNr = 0;
		}

		league.setClub(clubNr);

		System.out.print("Update Nationality: ");

		try {
			league.setNationality(inputid.readLine());
		} catch (Exception e) {
		}

		LeagueRepository leagueRepository = new MysqlLeagueRepository();
		leagueRepository.update(id, league);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*               UPDATE LEAGUES                *");
		System.out.println("***********************************************");
		System.out.println("*              Update Completed               *");
		System.out.println("*           Push Any Button For Back          *");
		System.out.println("***********************************************");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printLeague("");
	}

	public void deleteLeague() {

		leagues = mlr.getAll();

		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println(" Choice League id:");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id = 0;
		}

		LeagueRepository leagueRepository = new MysqlLeagueRepository();
		leagueRepository.delete(id);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*               DELETE LEAGUES                *");
		System.out.println("***********************************************");
		System.out.println("*              Delete Completed               *");
		System.out.println("*           Push Any Button For Back          *");
		System.out.println("***********************************************");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printLeague("");
	}

	//////////////////////////////////////////// CLUB////////////////////////////////////////////////////

	public void printClub(String message) {

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*                MENU CLUBS                   *");
		System.out.println("*                                             *");
		System.out.println("*           1       Show List                 *");
		System.out.println("*           2       View Club's Footballers   *");
		System.out.println("*           3       View Club's League        *");
		System.out.println("*           4       Back Start Menu           *");
		System.out.println("*                                             *");
		System.out.println("*************** Insert Choice  ****************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewAllClub();
			break;
		case 2:
			this.clear();
			this.viewFootClub();
			break;
		case 3:
			this.clear();
			this.viewLeague();
			break;
		case 4:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printClub("Error Choice");
		}
	}

	public void addClub() {
		Club club = new Club();

		System.out.println("***********************************************");
		System.out.println("*                INSERT CLUBS                 *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.print("Insert Name: ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {
			String nameC = input.readLine();
			club.setName(nameC);
			leagues = mlr.getAll();

			this.clear();

			
			System.out.println("**************************************************************");
			System.out.println("*                          LEAGUES                           *");
			System.out.println("*                                                            *");
			System.out.println("* Id    Name               Nr Club      Nationality          *");
			System.out.println("*                                                            *");
			System.out.println("**************************************************************");

			for (int i = 0; i < leagues.size(); i++) {
				System.out.println(leagues.get(i).toString());
			}

			System.out.println("*                                                            *");
			System.out.println("**************************************************************");
			System.out.print("Insert Id League: ");

			int leagueC;

			try {
				leagueC = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				leagueC = 0;
			}

			club.setIdLeague(leagueC);

			System.out.print("Insert President: ");

			String president = input.readLine();
			club.setPresident(president);

			ClubRepository clubRepository = new MysqlClubRepository();

			clubRepository.add(club);

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                INSERT CLUBS                 *");
			System.out.println("***********************************************");
			System.out.println("*               Entry Completed               *");
			System.out.println("*           Push Any Button For Back          *");
			System.out.println("***********************************************");

			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printClub("");

	}

	public void viewAllClub() {

		clubs = mcr.getAll();

		this.clear();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("Push Any Button For Back");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printClub("");
	}

	public void viewLeague() {

		clubs = mcr.getAll();

		this.clear();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("Choice Club Id: ");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		int idClub;
		try {
			idClub = Integer.parseInt(inputid.readLine());

			leagues = mlr.getLeague(idClub);
		} catch (Exception e) {
			idClub = 0;
		}
		this.clear();

		
		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println("Push Any Button For Back");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printLeague("");
	}

	public void updateClub() {

		clubs = mcr.getAll();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println(" Choice Club id:");

		Club club = new Club();

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id = 0;
		}

		System.out.println("***********************************************");
		System.out.println("*                UPDATE CLUBS                 *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.print("Update Name: ");

		try {
			club.setName(inputid.readLine());
		} catch (Exception e) {
		}

		leagues = mlr.getAll();

		this.clear();
		
		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		System.out.print("Update Id League: ");

		int idLeague;

		try {
			idLeague = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			idLeague = 0;
		}

		club.setIdLeague(idLeague);

		System.out.print("Update President: ");
		try {
			club.setPresident(inputid.readLine());
		} catch (Exception e) {
		}

		ClubRepository clubRepository = new MysqlClubRepository();
		clubRepository.update(id, club);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*                UPDATE CLUBS                 *");
		System.out.println("***********************************************");
		System.out.println("*              Update Completed               *");
		System.out.println("*           Push Any Button For Back          *");
		System.out.println("***********************************************");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printClub("");
	}

	public void deleteClub() {

		clubs = mcr.getAll();
		this.clear();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println(" Choice Club id:");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id = 0;
		}

		ClubRepository clubRepository = new MysqlClubRepository();
		clubRepository.delete(id);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*                DELETE CLUBS                 *");
		System.out.println("***********************************************");
		System.out.println("*              Delete Completed               *");
		System.out.println("*           Push Any Button For Back          *");
		System.out.println("***********************************************");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printClub("");
	}

	//////////////////////////////////////////////// FOOTBALLER/////////////////////////////////////////////////////////////

	public void printFootballer(String message) {

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*             MENU FOOTBALLERS                *");
		System.out.println("*                                             *");
		System.out.println("*         1       Show List                   *");
		System.out.println("*         2       Footballer's Club & League  *");
		System.out.println("*         3       Back Start Menu             *");
		System.out.println("*                                             *");
		System.out.println("*************** Insert Choice  ****************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewAllFootballer();
			break;
		case 2:
			this.clear();
			this.viewClubEleague();
			break;
		case 3:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printFootballer("Error Choice");
		}
	}

	public void addFootballer() {
		Footballer footballer = new Footballer();

		System.out.println("***********************************************");
		System.out.println("*             INSERT FOOTBALLERS              *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.print("Insert Name: ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {
			String nameF = input.readLine();
			footballer.setName(nameF);

			System.out.print("Insert Surname: ");

			String surnameF = input.readLine();
			footballer.setSurname(surnameF);

			System.out.print("Insert Number: ");

			int number;

			try {
				number = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				number = 0;
			}

			footballer.setNumber(number);

			System.out.print("Insert Foot(R/L): ");

			String foot = input.readLine();
			footballer.setFoot(foot);

			System.out.print("Insert Position(G/D/M/F): ");

			String position = input.readLine();
			footballer.setPosition(position);

			System.out.print("Insert Nationality: ");

			String nationalityF = input.readLine();
			footballer.setNationality(nationalityF);
			clubs = mcr.getAll();
			this.clear();

			System.out.println("*********************************************************************");
			System.out.println("*                              CLUBS                                *");
			System.out.println("*                                                                   *");
			System.out.println("* Id   Name               League               President            *");
			System.out.println("*                                                                   *");
			System.out.println("*********************************************************************");

			for (int i = 0; i < clubs.size(); i++) {
				System.out.println(clubs.get(i).toString());
			}

			System.out.println("*                                                                   *");
			System.out.println("*********************************************************************");
			System.out.print("Insert Id Club: ");

			int clubF;

			try {
				clubF = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				clubF = 0;
			}

			footballer.setIdClub(clubF);

			FootballerRepository footballerRepository = new MysqlFootballerRepository();

			footballerRepository.add(footballer);

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*             INSERT FOOTBALLERS              *");
			System.out.println("***********************************************");
			System.out.println("*               Entry Completed               *");
			System.out.println("*           Push Any Button For Back          *");
			System.out.println("***********************************************");

			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printFootballer("");

	}

	public void viewAllFootballer() {

		footballers = mfr.getAll();

		this.clear();

		System.out.println("******************************************************************************************************************************");
		System.out.println("*                                                    FOOTBALLERS                                                             *");
		System.out.println("*                                                                                                                            *");
		System.out.println("* Id     Name                  Surname                 Nr      Foot    Pos.     Nat.                    Club                 *");
		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");

		for (int i = 0; i < footballers.size(); i++) {
			System.out.println(footballers.get(i).toString());
		}

		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");
		System.out.println("Push Any Button For Back");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printFootballer("");
	}

	public void viewClubEleague() {

		footballers = mfr.getAll();
		this.clear();

		System.out.println("******************************************************************************************************************************");
		System.out.println("*                                                    FOOTBALLERS                                                             *");
		System.out.println("*                                                                                                                            *");
		System.out.println("* Id     Name                  Surname                 Nr      Foot    Pos.     Nat.                    Club                 *");
		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");

		for (int i = 0; i < footballers.size(); i++) {
			System.out.println(footballers.get(i).toString());
		}

		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");
		System.out.println("Choice Footballer id: ");
		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		int idF;
		try {
			idF = Integer.parseInt(inputid.readLine());

		} catch (Exception e) {
			idF = 0;
		}
		clubs = mcr.getClubEleague(idF);

		this.clear();
		
		System.out.println("***************************************************");
		System.out.println("*                  CLUBS & LEAGUES                *");
		System.out.println("*                                                 *");
		System.out.println("* Id    Name Club            Name League          *");
		System.out.println("*                                                 *");
		System.out.println("***************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toStringClubEleague());
		}

		System.out.println("*                                                 *");
		System.out.println("***************************************************");
		System.out.println("Push Any Button For Back");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printFootballer("");
	}

	public void viewFootClub() {

		clubs = mcr.getAll();

		this.clear();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("Choice Club Id: ");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		int idC;

		try {
			idC = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			idC = 0;
		}
		footballers = mfr.getFootClub(idC);

		this.clear();
		
		System.out.println("******************************************************************************************************************************");
		System.out.println("*                                                    FOOTBALLERS                                                             *");
		System.out.println("*                                                                                                                            *");
		System.out.println("* Id     Name                  Surname                 Nr      Foot    Pos.     Nat.                    Club                 *");
		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");

		for (int i = 0; i < footballers.size(); i++) {
			System.out.println(footballers.get(i).toString());
		}

		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");
		System.out.println("Push Any Button For Back");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printClub("");
	}

	public void updateFootballer() {

		footballers = mfr.getAll();
		this.clear();

		System.out.println("******************************************************************************************************************************");
		System.out.println("*                                                    FOOTBALLERS                                                             *");
		System.out.println("*                                                                                                                            *");
		System.out.println("* Id     Name                  Surname                 Nr      Foot    Pos.     Nat.                    Club                 *");
		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");

		for (int i = 0; i < footballers.size(); i++) {
			System.out.println(footballers.get(i).toString());
		}

		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");
		System.out.println(" Choice Footballer id:");

		Footballer footballer = new Footballer();

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(input.readLine());
		} catch (Exception e) {
			id = 0;
		}

		try {
			System.out.print("Insert Name: ");

			String nameF = input.readLine();
			footballer.setName(nameF);

			System.out.print("Insert Surname: ");

			String surnameF = input.readLine();
			footballer.setSurname(surnameF);
		} catch (Exception e) {
		}

		System.out.print("Insert Number: ");

		int number;

		try {
			number = Integer.parseInt(input.readLine());
		} catch (Exception e) {
			number = 0;
		}

		try {
			footballer.setNumber(number);

			System.out.print("Insert Foot: ");

			String foot = input.readLine();
			footballer.setFoot(foot);

			System.out.print("Insert Position: ");

			String position = input.readLine();
			footballer.setPosition(position);

			System.out.print("Insert Nationality: ");

			String nationalityF = input.readLine();
			footballer.setNationality(nationalityF);
		} catch (Exception e) {
		}

		clubs = mcr.getAll();
		this.clear();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.print("Insert Id Club: ");

		int clubF;
		try {
			clubF = Integer.parseInt(input.readLine());

		} catch (Exception e) {
			clubF = 0;
		}

		footballer.setIdClub(clubF);

		FootballerRepository footballerRepository = new MysqlFootballerRepository();

		footballerRepository.update(id, footballer);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*             UPDATE FOOTBALLERS              *");
		System.out.println("***********************************************");
		System.out.println("*               Update Completed              *");
		System.out.println("*           Push Any Button For Back          *");
		System.out.println("***********************************************");

		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printFootballer("");
	}

	public void deleteFootballer() {

		clubs = mcr.getAll();

		this.clear();

		System.out.println("******************************************************************************************************************************");
		System.out.println("*                                                    FOOTBALLERS                                                             *");
		System.out.println("*                                                                                                                            *");
		System.out.println("* Id     Name                  Surname                 Nr      Foot    Pos.     Nat.                    Club                 *");
		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");

		for (int i = 0; i < footballers.size(); i++) {
			System.out.println(footballers.get(i).toString());
		}

		System.out.println("*                                                                                                                            *");
		System.out.println("******************************************************************************************************************************");
		System.out.println(" Choice Footballer id:");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id = 0;
		}

		FootballerRepository footballerRepository = new MysqlFootballerRepository();
		footballerRepository.delete(id);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*             DELETE FOOTBALLERS              *");
		System.out.println("***********************************************");
		System.out.println("*               Delete Completed              *");
		System.out.println("*           Push Any Button For Back          *");
		System.out.println("***********************************************");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printFootballer("");
	}

}