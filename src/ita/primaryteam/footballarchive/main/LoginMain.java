package ita.primaryteam.footballarchive.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import ita.primaryteam.footballarchive.domain.User;
import ita.primaryteam.footballarchive.repository.MysqlUserRepository;

public class LoginMain {
	
		MysqlUserRepository mur = new MysqlUserRepository();
	
		User user = new User();
		MenuAdmin mAdmin = new MenuAdmin();
		MenuUser  mUser = new MenuUser();
		
		public static void main(String[] args) {
			LoginMain menu = new LoginMain();
			menu.printLogin("");
		}
		
		public void printLogin(String message) {

			System.out.println("***********************************************");
			System.out.println("*                   LOGIN                     *");
			System.out.println("*                                             *");
			System.out.println("*                                             *");
			System.out.println("*                                             *");
			System.out.println("***********************************************");
			System.out.println(message);
			
			System.out.println("Insert Username: ");

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			try {
				user.setUsername(input.readLine());
			} catch (Exception e) {
	
			}
			
			System.out.println("Insert Password: ");

			try {
				user.setPassword(input.readLine());
			} catch (Exception e) {
	
			}
			
			user = mur.getLogin(user);
			System.out.println(user.getLevel());
			
			switch (user.getLevel()) {
			case "admin":
				mAdmin.clear();
				mAdmin.firstMenu(user.getUsername(),"");
				break;
			case "user":
				mAdmin.clear();
				mUser.firstMenu(user.getUsername(),"");
				break;
			case "User Unknown":
				mAdmin.clear();
				this.printLogin("User Unknown");
				break;
			case "Password Error":
				mAdmin.clear();
				this.printLogin("Password Error");
				break;
			default:
				mAdmin.clear();
			}
		}
}

