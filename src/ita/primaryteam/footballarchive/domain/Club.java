package ita.primaryteam.footballarchive.domain;

public class Club extends Football {

	private String name;
	private League league = new League();
	private String president;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getIdLeague() {
		return this.league.getId();
	}
	public void setIdLeague(int id) {
		this.league.setId(id);
	}

	public String getPresident() {
		return president;
	}
	public void setPresident(String president) {
		this.president = president;
	}

	public void setLeague(League league) {
		this.league= league;
	}

	public League getLeague() {
		return this.league;
	}

	@Override
	public String toString() {

		int l1 = this.name.length();
		if(l1 < 20) {
			for( int i=0; i<(20-l1); i++) {
				this.name = this.name + " ";
			}
		}

		int l2 = this.president.length();
		if(l2 < 20) {
			for( int i=0; i<(20-l2); i++) {
				this.president = this.president + " ";
			}
		}

		int l3 = this.league.getName().length();
		if(l3 < 20) {
			for( int i=0; i<(20-l3); i++) {
				this.league.setName(this.league.getName() + " ");
			}
		}


		return "* " +this.id + " " + this.name + " " + this.league.getName() + " " + this.president + " *";
	}

	public String toStringClubEleague() {

		int l1 = this.name.length();
		if(l1 < 20) {
			for( int i=0; i<(20-l1); i++) {
				this.name = this.name + " ";
			}
		}

		int l3 = this.league.getName().length();
		if(l3 < 20) {
			for( int i=0; i<(20-l3); i++) {
				this.league.setName(this.league.getName() + " ");
			}
		}


		return "* " +this.id + "\t" + this.name + " " + this.league.getName() + " *";
	}
}
