package ita.primaryteam.footballarchive.domain;

public class User extends Football {
	
	private String username;
	private String password;
	private String level;
	//private MenuAdmin menu;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	@Override
	public String toString() {
		return id + " " + getUsername() + " " + getPassword() + " " + getLevel();
	}

}
