package ita.primaryteam.footballarchive.domain;

public abstract class Football {
	
	protected int id;
	protected boolean visibility = true;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isVisibility() {
		return visibility;
	}
	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
	
}
