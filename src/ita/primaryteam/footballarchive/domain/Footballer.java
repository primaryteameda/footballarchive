package ita.primaryteam.footballarchive.domain;

public class Footballer extends Football {

	private String name;
	private String surname;
	private int number;
	private String foot;
	private String position;
	private String nationality;
	private Club club = new Club();

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getFoot() {
		return foot;
	}
	public void setFoot(String foot) {
		this.foot = foot;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public int getIdClub() {
		return this.club.getId();
	}
	public void setIdClub(int idClub) {
		this.club.setId(idClub);
	}

	public void setClub(Club club) {
		this.club=club;
	}

	public Club getClub() {
		return this.club;
	}

	@Override
	public String toString() {

		int l1 = this.name.length();
		if(l1 < 20) {
			for( int i=0; i<20-l1; i++) {
				this.name = this.name + " ";
			}
		}

		int l2 = this.surname.length();
		if(l2 < 20) {
			for( int i=0; i<20-l2; i++) {
				this.surname = this.surname + " ";
			}
		}

		int l3 = this.nationality.length();
		if(l3 < 20) {
			for( int i=0; i<20-l3; i++) {
				this.nationality = this.nationality + " ";
			}
		}
		int l=this.club.getName().length();
		if(l < 20) {
			for( int i=0; i<20-l; i++) {
				this.club.setName(this.club.getName()+ " ");
			}
		}

		return "* " + this.id + "\t" + this.name + "\t" + this.surname + "\t" + this.number + "\t" + this.foot 
				+ "\t" + this.position + "\t" + this.nationality + "\t" + this.club.getName() + " *";
	}

	public String toStringFootballer() {

		int l1 = this.name.length();
		if(l1 < 20) {
			for( int i=0; i<20-l1; i++) {
				this.name = this.name + " ";
			}
		}

		int l2 = this.surname.length();
		if(l2 < 20) {
			for( int i=0; i<20-l2; i++) {
				this.surname = this.surname + " ";
			}
		}

		int l=this.club.getName().length();
		if(l < 20) {
			for( int i=0; i<20-l; i++) {
				this.club.setName(this.club.getName()+ " ");
			}
		}

		return "* " + this.id + "\t" + this.name + "\t" + this.surname+ "\t"  + this.club.getName() + " *";
	}

}
