package ita.primaryteam.footballarchive.domain;

public class League extends Football {

	private String name;
	private int club;
	private String nationality;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getClub() {
		return this.club;
	}
	public void setClub(int club) {
		this.club=club;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	@Override
	public String toString() {
		int l1 = this.name.length();
		if(l1 < 20) {
			for( int i=0; i<20-l1; i++) {
				this.name = this.name + " ";
			}
		}
		int l2 = this.nationality.length();
		if(l2 < 20) {
			for( int i=0; i<20-l2; i++) {
				this.nationality = this.nationality + " ";
			}
		}
		return "* " +this.id + "\t" + this.name + " " + this.club + "\t\t" + this.nationality + " *";
	}

}
