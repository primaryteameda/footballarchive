package ita.primaryteam.footballarchive.repository;

import java.util.List;

import ita.primaryteam.footballarchive.domain.Footballer;

public interface FootballerRepository {

	List<Footballer> getAll();

	void add(Footballer footballer);

	void update(int id, Footballer footballer);

	void delete(int id);

	void deleteAll();

	List<Footballer> getFootballers(int id);

	List<Footballer> getFootClub(int id);

	List<Footballer> getInvisible();

}
