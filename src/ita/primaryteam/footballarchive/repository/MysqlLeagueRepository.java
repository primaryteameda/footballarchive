package ita.primaryteam.footballarchive.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import ita.primaryteam.footballarchive.domain.Club;
import ita.primaryteam.footballarchive.domain.League;

public class MysqlLeagueRepository implements LeagueRepository {

	private static final String URL = "jdbc:mysql://localhost:3306/football";
	private static final String USER = "root";
	private static final String PASSWORD = "test";

	@Override
	public void add(League league) {
		league.setVisibility(true);
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO Leagues(name, club, nationality, visibility) " + 
					"VALUES ('%s', %d, '%s', %b)", league.getName(),
					league.getClub(), league.getNationality(), league.isVisibility());
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	@Override
	public List<League> getAll() {
		
		List<League> leagues = new ArrayList<League>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Leagues WHERE visibility = true");
			while (resultSet.next()) {
				
				League league = new League();
				league.setId(resultSet.getInt("id"));
				league.setName(resultSet.getString("name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("nationality"));
				league.setVisibility(true);
				leagues.add(league);
			
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return leagues;
	}
	
	@Override
	public List<League> getInvisible() {
		List<League> leagues = new ArrayList<League>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Leagues WHERE visibility = false");
			while (resultSet.next()) {
				
				League league = new League();
				
				league.setId(resultSet.getInt("id"));
				league.setName(resultSet.getString("name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("nationality"));
				league.setVisibility(false);
				leagues.add(league);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return leagues;
	}

	@Override
	public List<League> getLeague(int id) {
		List<League> leagues = new ArrayList<League>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			
			Club club = new Club();
			League league = new League();
			
			league.setVisibility(true);
			club.setVisibility(true);
			
			//dato il club da la lega
			ResultSet resultSet = statement.executeQuery("SELECT * FROM leagues l INNER JOIN clubs as c ON l.Id=c.IdLeague where c.id=" + id 
														+ " AND c.visibility="+ club.isVisibility());
			while (resultSet.next()) {

				

				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("l.nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				leagues.add(league);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return leagues;
	}

	@Override
	public void update(int id, League league) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			league.setVisibility(true);
			String sql = String.format("UPDATE Leagues l" +
					"  SET l.name = '%s'," +
					"	  l.club = %d  ," +
					"      l.nationality = '%s', " +
					"        l.visibility = %b " +
					"WHERE l.id = %d", league.getName(),
					league.getClub(), league.getNationality(), league.isVisibility(), id);

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int id) {
		League league = new League();
		
		league.setVisibility(false);
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			//String sql = String.format("DELETE FROM Leagues WHERE id = %d", id);
			String sql = String.format("UPDATE Leagues l SET visibility = %b WHERE id = %d", league.isVisibility(), id);

			statement.executeUpdate(sql);

			sql = String.format("UPDATE Clubs SET idleague=0 WHERE idleague = %d", id);

			statement.executeUpdate(sql);


		} catch (SQLException e) {
			System.out.println("SQL exception");
		}
	}

	@Override
	public void deleteAll() {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM Leagues");

			statement.executeUpdate(sql);


		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
}
