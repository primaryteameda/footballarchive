package ita.primaryteam.footballarchive.repository;

import java.util.List;

import ita.primaryteam.footballarchive.domain.Club;

public interface ClubRepository {

	List<Club> getAll();

	void add(Club club);

	void update(int id, Club club);

	void delete(int id);

	void deleteAll();

	List<Club> getInvisible();

	List<Club> getClubs(int id);
}
