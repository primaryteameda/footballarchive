package ita.primaryteam.footballarchive.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ita.primaryteam.footballarchive.domain.Club;
import ita.primaryteam.footballarchive.domain.Footballer;
import ita.primaryteam.footballarchive.domain.League;

public class MysqlClubRepository implements ClubRepository{

	private static final String URL = "jdbc:mysql://localhost:3306/football";
	private static final String USER = "root";
	private static final String PASSWORD = "test";

	@Override
	public void add(Club club) {
		club.setVisibility(true);
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO Clubs(name, idleague, president, visibility) " + 
					"VALUES ('%s', %d, '%s', %b)", club.getName(),
					club.getIdLeague(), club.getPresident(), club.isVisibility());
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	@Override
	public List<Club> getAll() {
		List<Club> clubs = new ArrayList<Club>();
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM Clubs as c WHERE c.idleague=0 AND c.visibility=true");
			ResultSet resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				
				Club club = new Club();
				League league = new League();
				
				league.setId(0);
				league.setName("");
				league.setClub(0);
				league.setNationality("");
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				clubs.add(club);
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM Clubs as c, Leagues as l WHERE c.idleague=l.id AND c.visibility=true");
			ResultSet resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				
				Club club = new Club();
				League league = new League();
				
				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				clubs.add(club);
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return clubs;
	}
	
	@Override
	public List<Club> getInvisible() {
		List<Club> clubs = new ArrayList<Club>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Clubs as c, Leagues as l WHERE c.idleague=l.id AND c.visibility=false");

			while (resultSet.next()) {
				Club club = new Club();
				League league = new League();
				
				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(false);

				clubs.add(club);
				
			}
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return clubs;
	}

	@Override
	public List<Club> getClubs(int id) {
		List<Club> clubs = new ArrayList<Club>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			//data una lega restituisce tutti i club
			ResultSet resultSet = statement.executeQuery("SELECT * FROM clubs c INNER JOIN leagues as l ON l.id=c.idleague where l.id =" + id 
														+ " AND c.visibility=true");
			while (resultSet.next()) {
			
				Club club = new Club();
				League league = new League();
				
				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				clubs.add(club);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return clubs;
	}

	public List<Club> getClubEleague(int id) {
		List<Club> clubs = new ArrayList<Club>();
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();

			String sql = "SELECT * FROM clubs as c INNER JOIN footballers as f ON c.id=f.idclub where c.idleague = 0 AND f.id =" + id 
						+ " AND c.visibility=true";


			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				
				Club club = new Club();
				League league = new League();
				Footballer footballer = new Footballer();

				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				league.setId(0);
				league.setName("");
				league.setClub(0);
				league.setNationality("");
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				clubs.add(club);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();

			//dato un giocatore restituisce club e lega 
			String sql = "SELECT * FROM leagues l INNER JOIN clubs as c ON l.id=c.idleague INNER JOIN footballers as f ON c.id=f.idclub where f.id =" + id 
						+ " AND c.visibility=true";


			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				
				Club club = new Club();
				League league = new League();
				Footballer footballer = new Footballer();

				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				clubs.add(club);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return clubs;
	}

	@Override
	public void update(int id, Club club) {
		
		club.setVisibility(true);
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Clubs c" +
					"  SET name = '%s'," +
					"	  idleague = %d, " +
					"   president = '%s', " +
					"   visibility = %b " +
					"WHERE c.id = %d", club.getName(),
					club.getIdLeague(), club.getPresident(), club.isVisibility(), id);

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int id) {
		
		Club club = new Club();
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			
			club.setVisibility(false);
			
			//String sql = String.format("DELETE FROM Clubs WHERE id = %d", id);
			String sql = String.format("UPDATE Clubs c SET c.visibility = %b WHERE id = %d",club.isVisibility(), id);
			

			statement.executeUpdate(sql);

			sql = String.format("UPDATE Footballers SET idclub=0 WHERE idclub = %d", id);

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
		}
	}

	@Override
	public void deleteAll() {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM Clubs");

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

}
