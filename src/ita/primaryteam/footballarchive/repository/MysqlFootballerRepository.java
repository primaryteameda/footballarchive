package ita.primaryteam.footballarchive.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ita.primaryteam.footballarchive.domain.Club;
import ita.primaryteam.footballarchive.domain.Footballer;
import ita.primaryteam.footballarchive.domain.League;

public class MysqlFootballerRepository implements FootballerRepository {

	private static final String URL = "jdbc:mysql://localhost:3306/football";
	private static final String USER = "root";
	private static final String PASSWORD = "test";

	@Override
	public void add(Footballer footballer) {
		
		footballer.setVisibility(true);
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO Footballers(name, surname, number, foot, position, nationality, idclub, visibility) " + 
					"VALUES ('%s', '%s', '%d', '%s', '%s', '%s', %d, %b)", footballer.getName(),
					footballer.getSurname(), footballer.getNumber(), footballer.getFoot(), 
					footballer.getPosition(), footballer.getNationality(), footballer.getIdClub(), footballer.isVisibility());
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	@Override
	public List<Footballer> getAll() {
		
		List<Footballer> footballers = new ArrayList<Footballer>();
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers f WHERE f.idclub=0 AND f.visibility=true");

			while (resultSet.next()) {
				
				Footballer footballer = new Footballer();
				Club club = new Club();
				League league = new League();
				
				league.setId(0);
				league.setName("");
				league.setClub(0);
				league.setNationality("");
				league.setVisibility(true);

				club.setId(0);
				club.setName("");
				club.setLeague(league);
				club.setPresident("");
				club.setVisibility(true);

				footballer.setId(resultSet.getInt("f.id"));
				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				footballers.add(footballer);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}


		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers f, clubs c, leagues l WHERE f.idclub= c.id AND c.idleague=l.id "
														+ " AND f.visibility=true");

			while (resultSet.next()) {
				
				Footballer footballer = new Footballer();
				Club club = new Club();
				League league = new League();
				
				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("l.nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				footballer.setId(resultSet.getInt("f.id"));
				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				footballers.add(footballer);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers f, clubs c WHERE f.idclub = c.id AND c.idleague=0 "
														+ " AND f.visibility=true");

			while (resultSet.next()) {
				
				Footballer footballer = new Footballer();
				Club club = new Club();
				League league = new League();
				
				league.setId(0);
				league.setName("");
				league.setClub(0);
				league.setNationality("");
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				footballer.setId(resultSet.getInt("f.id"));
				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				footballers.add(footballer);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return footballers;
	}
	
	@Override
	public List<Footballer> getInvisible() {
		
		List<Footballer> footballers = new ArrayList<Footballer>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers f, clubs c, leagues l WHERE f.idclub= c.id AND c.idleague=l.id "
														+ " AND f.visibility=false");

			while (resultSet.next()) {
			
				Footballer footballer = new Footballer();
				Club club = new Club();
				League league = new League();
				
				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("l.nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				footballer.setId(resultSet.getInt("f.id"));
				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(false);

				footballers.add(footballer);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return footballers;
	}

	@Override
	public List<Footballer> getFootballers(int id) {
		
		List<Footballer> footballers = new ArrayList<Footballer>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			
			//data una lega restituisce tutti i giocatori
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers as f "
					+ "INNER JOIN clubs as c ON c.id=f.idclub INNER JOIN leagues as l ON l.id=c.idleague "
					+ "where l.id =" + id + " AND f.visibility=true");
			
			while (resultSet.next()) {

				Footballer footballer = new Footballer();
				Club club = new Club();
				League league = new League();
				
				footballer.setId(resultSet.getInt("f.id"));
				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("l.nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				footballers.add(footballer);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return footballers;
	}

	@Override
	public List<Footballer> getFootClub(int id) {
		List<Footballer> footballers = new ArrayList<Footballer>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			
			//dato un club restituisce tutti i giocatori
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers f, clubs c, leagues l where f.Idclub=c.Id and c.idleague=l.id and c.id=" + id 
														+ " AND f.visibility=true");

			while (resultSet.next()) {

				Footballer footballer = new Footballer();
				Club club = new Club();
				League league = new League();
				
				footballer.setId(resultSet.getInt("f.id"));
				footballer.setName(resultSet.getString("f.name"));
				footballer.setSurname(resultSet.getString("surname"));
				footballer.setNumber(resultSet.getInt("number"));
				footballer.setFoot(resultSet.getString("foot"));
				footballer.setPosition(resultSet.getString("position"));
				footballer.setNationality(resultSet.getString("f.nationality"));
				footballer.setClub(club);
				footballer.setVisibility(true);

				league.setId(resultSet.getInt("l.id"));
				league.setName(resultSet.getString("l.name"));
				league.setClub(resultSet.getInt("club"));
				league.setNationality(resultSet.getString("l.nationality"));
				league.setVisibility(true);

				club.setId(resultSet.getInt("c.id"));
				club.setName(resultSet.getString("c.name"));
				club.setLeague(league);
				club.setPresident(resultSet.getString("president"));
				club.setVisibility(true);

				footballers.add(footballer);

			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return footballers;
	}

	@Override
	public void update(int id, Footballer footballer) {
		
		footballer.setVisibility(true);
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Clubs c" +
					"  SET name = '%s'," +
					"	  surname = '%s'  ," +
					"   number = %d, " +
					"   foot = '%s', " +
					"   position = '%s', " +
					"   nationality = '%s', " +
					"   idclub = %d " +
					"   visibility = %b " +
					"WHERE c.id = %d", footballer.getName(),
					footballer.getSurname(), footballer.getNumber(), 
					footballer.getFoot(), footballer.getPosition(), 
					footballer.getNationality(), footballer.getIdClub(), footballer.isVisibility(), id);

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int id) {
		
		Footballer footballer = new Footballer();
		
		footballer.setVisibility(false);
		
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			
			//String sql = String.format("DELETE FROM Footballers WHERE id = %d", id);
			String sql = String.format("UPDATE Footballers f SET f.visibility = %b WHERE id = %d", footballer.isVisibility(), id);

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
		}
	}

	@Override
	public void deleteAll() {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM Footballers");

			statement.executeUpdate(sql);


		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}


}
