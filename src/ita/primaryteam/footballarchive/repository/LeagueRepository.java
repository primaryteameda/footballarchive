package ita.primaryteam.footballarchive.repository;

import java.util.List;
import ita.primaryteam.footballarchive.domain.League;

public interface LeagueRepository {

	List<League> getAll();

	void add(League league);

	void update(int id, League league);

	void delete(int id);

	void deleteAll();

	List<League> getLeague(int id);

	List<League> getInvisible();

}
