package ita.primaryteam.footballarchive.repository;

import ita.primaryteam.footballarchive.domain.User;

public interface UserRepository {

	User getLogin(User user);

}
