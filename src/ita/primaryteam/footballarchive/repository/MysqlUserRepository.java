package ita.primaryteam.footballarchive.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ita.primaryteam.footballarchive.domain.User;

public class MysqlUserRepository implements UserRepository {
	
	
	
	private static final String URL = "jdbc:mysql://localhost:3306/football";
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	
	@SuppressWarnings("resource")
	@Override
	public User getLogin(User user) {
		
	
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			
			//user sbagliato
			user.setId(user.getId());
			user.setUsername(user.getUsername());
			user.setPassword(user.getPassword());
			user.setLevel("User Unknown");
			user.setVisibility(false);
			
			// uno dei due errati
			String sql = ("SELECT *  FROM Users as u WHERE u.visibility = true AND u.username = '" + user.getUsername() + "'");
			ResultSet resultSet = statement.executeQuery(sql);
			
			
			while (resultSet.next()) {
				
				//username giusto pass sbagliata
				user.setId(user.getId());
				user.setUsername(user.getUsername());
				user.setPassword(user.getPassword());
				user.setLevel("Password Error");
				user.setVisibility(false);
			
			
			 sql = "SELECT *  FROM Users as u WHERE u.visibility = true AND u.username ='" + user.getUsername() 
			+ "' AND u.password ='"  + user.getPassword() + "'";
			resultSet = statement.executeQuery(sql);	
			
			while (resultSet.next()) {
				// user giusto pass giusta
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));
				user.setLevel(resultSet.getString("level"));
				user.setVisibility(true);
		
		}
			}
		}
				catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			System.out.println("ggggg");
		}
	

		return user;
		}

}


