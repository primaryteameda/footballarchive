
The team has developed a management related to a football archive.
The main scope of this management is to allow the user administrate of footballers and their properties, their membership clubs and the leagues where the clubs are belong. 

The management provides a collection of data divided into: Leagues, Clubs and Footballers


The Leagues have these properties:id,league name,clubs number,nationality.
The Clubs have these properties: id,club name,league of belonging, nationality.
And finally the Footballers have these properties:id,name and surname footballer,shirt number,foot,position,nationality and club membership. 

The user login the system by name and password to execute operations if administrator, else he can only view.

The system allows the administrator to add data, view the entire contents of the archive or a single element inserted in it, through combined searches between the various entities.
The user can view the club and the league to which a footballer belongs; view all the clubs or all the footballers in a league;
view all the footballers or the league to which only one club belongs. 
Moreover, the user can modify or delete the content present in the archive.